using FluentAssertions;
using OpenQA.Selenium;


namespace Pages
{
    public class LandingPage
    {
        private const string Url = "https://www.sovtech.co.uk/contact-us";
        private static readonly IWebDriver Driver = GaugeSupport.Driver;


        public LandingPage OpenLandingPage()
        {
            Driver.Navigate().GoToUrl(Url);
            var title = driver.Title;
            Assert.That(title, Is.EqualTo("Contact Us - SovTech Bespoke Software Development"));
            return this;
        }
    }
}
