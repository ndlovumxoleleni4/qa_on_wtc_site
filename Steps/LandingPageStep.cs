using Gauge.CSharp.Lib.Attribute;
using Pages;

namespace Steps
{
    public class LandingPageStep
    {
        private readonly LandingPage landingPage = new LandingPage();


        [Step("Open The SovTech Contact Us Page.")]
        public void GoToLandingPage() => landingPage.OpenLandingPage();
    }
}
